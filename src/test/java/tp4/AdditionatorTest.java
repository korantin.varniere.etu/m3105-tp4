package tp4;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class AdditionatorTest {

	@Test
	public void empty_string_return_zero() {
		assertEquals("0.0", Additionator.additionator(""));
	}
	
	@Test
	public void one_line_string_return_sum() {
		assertEquals("3.0", Additionator.additionator("1.0,2.0"));
		assertEquals("1.0", Additionator.additionator("0.0,1.0"));
		assertEquals("7.0", Additionator.additionator("5.0,2.0"));
	}
	
	@Test
	public void numbers_can_be_reals() {
		assertEquals("3.4", Additionator.additionator("1.1,2.3"));
		assertEquals("2.2", Additionator.additionator("0.8,1.4"));
		assertEquals("7.2", Additionator.additionator("5.1,2.1"));
	}
	
	@Test
	public void string_cant_finish_by_coma() {
		assertEquals("Nombre attendu en fin de chaîne", Additionator.additionator("1.1,2.3,"));
		assertEquals("Nombre attendu en fin de chaîne", Additionator.additionator("0.8,1.4,"));
		assertEquals("Nombre attendu en fin de chaîne", Additionator.additionator("5.1,2.1,"));
	}
	
	@Test
	public void numbers_can_be_separated_by_carriage_return() {
		assertEquals("3.4", Additionator.additionator("1.1\n2.3"));
		assertEquals("2.2", Additionator.additionator("0.8\n1.4,0.0"));
		assertEquals("7.2", Additionator.additionator("5.1\n2.1"));
	}
	
	@Test
	public void numbers_cant_be_separated_by_two_separators() {
		assertEquals("Nombre attendu à la place de \"\\n\" à la position 4", Additionator.additionator("1.1,\n2.3"));
		assertEquals("Nombre attendu à la place de \"\\n\" à la position 8", Additionator.additionator("0.8,1.4,\n0.0"));
		assertEquals("Nombre attendu à la place de \"\\n\" à la position 4", Additionator.additionator("5.1,\n2.1"));
	}
	
	@Test
	public void we_can_specify_a_custom_separator() {
		assertEquals("20.7", Additionator.additionator("//|\\n15.0|2.0|3.7"));
		assertEquals("5.0", Additionator.additionator("//blah\\n2.0blah3.0"));
		assertEquals("'|' attendu à la place de ',' à la position 7", Additionator.additionator("//|\\n1.0|2.0,3.0"));
	}
	
	@Test
	public void negative_numbers_are_not_allowed() {
		assertEquals("Négatif interdits : -1.0", Additionator.additionator("-1.0,2.3"));
		assertEquals("Négatif interdits : -5.0", Additionator.additionator("0.8,-5.0,0.0"));
		assertEquals("Négatif interdits : -6.0", Additionator.additionator("5.0,-6.0"));
	}
	
}
