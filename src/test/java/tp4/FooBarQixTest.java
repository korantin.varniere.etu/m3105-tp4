package tp4;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class FooBarQixTest {

	@Test
	public void return_string_of_number_when_no_rule_detected_test() {
		assertEquals("1", FooBarQix.fooBarQix(1));
		assertEquals("2", FooBarQix.fooBarQix(2));
		assertEquals("4", FooBarQix.fooBarQix(4));
		assertEquals("8", FooBarQix.fooBarQix(8));
	}
	
	@Test
	public void divide_by_three_test() {
		assertEquals("Foo", FooBarQix.fooBarQix(6));
		assertEquals("Foo", FooBarQix.fooBarQix(9));
		assertEquals("Foo", FooBarQix.fooBarQix(12));
		assertEquals("Foo", FooBarQix.fooBarQix(18));
	}
	
	@Test
	public void divide_by_five_test() {
		assertEquals("Bar", FooBarQix.fooBarQix(10));
		assertEquals("Bar", FooBarQix.fooBarQix(20));
		assertEquals("Bar", FooBarQix.fooBarQix(100));
		assertEquals("Bar", FooBarQix.fooBarQix(110));
	}
	
	@Test
	public void divide_by_seven_test() {
		assertEquals("Qix", FooBarQix.fooBarQix(14));
		assertEquals("Qix", FooBarQix.fooBarQix(28));
		assertEquals("Qix", FooBarQix.fooBarQix(49));
		assertEquals("Qix", FooBarQix.fooBarQix(91));
	}
	
	@Test
	public void divide_by_three_or_five_or_seven_test() {
		assertEquals("FooQix", FooBarQix.fooBarQix(21));
		assertEquals("BarQix", FooBarQix.fooBarQix(140));
		assertEquals("FooBar", FooBarQix.fooBarQix(60));
		assertEquals("FooBarQix", FooBarQix.fooBarQix(210));
	}
	
	@Test
	public void contains_three_test() {
		assertEquals("Foo", FooBarQix.fooBarQix(139));
		assertEquals("Foo", FooBarQix.fooBarQix(389));
		assertEquals("Foo", FooBarQix.fooBarQix(302));
		assertEquals("Foo", FooBarQix.fooBarQix(311));
	}
	
	@Test
	public void contains_five_test() {
		assertEquals("Bar", FooBarQix.fooBarQix(52));
		assertEquals("Bar", FooBarQix.fooBarQix(502));
		assertEquals("Bar", FooBarQix.fooBarQix(506));
		assertEquals("Bar", FooBarQix.fooBarQix(541));
	}
	
	@Test
	public void contains_seven_test() {
		assertEquals("Qix", FooBarQix.fooBarQix(71));
		assertEquals("Qix", FooBarQix.fooBarQix(79));
		assertEquals("Qix", FooBarQix.fooBarQix(97));
		assertEquals("Qix", FooBarQix.fooBarQix(947));
	}
	
	@Test
	public void all_test() {
		assertEquals("QixQixQix", FooBarQix.fooBarQix(77));
		assertEquals("FooFoo", FooBarQix.fooBarQix(3));
		assertEquals("BarBarQixBar", FooBarQix.fooBarQix(575));
		assertEquals("Foo", FooBarQix.fooBarQix(13));
		assertEquals("Bar", FooBarQix.fooBarQix(10));
		assertEquals("FooBarBar", FooBarQix.fooBarQix(15));
		assertEquals("1", FooBarQix.fooBarQix(1));
		assertEquals("2", FooBarQix.fooBarQix(2));
	}
	
}
