package tp4;

import java.util.Locale;

public class Additionator {

	public static String additionator(String exp) {
		if (exp.length() == 0) {
			return "0.0";
		}
		
		if (exp.charAt(exp.length()-1) == ',') {
			return "Nombre attendu en fin de chaîne";
		}
		
		String[] numbers = exp.split(",|\\n");
		
		if (numbers[0].contains("//")) {
			return customAdditionator(exp, numbers[0].substring(2, numbers[0].indexOf('\\')));
		}
		
		double sum = 0;
		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i].equals("")) {
				return numberSeparatorException(numbers, i);
			} else if (Double.parseDouble(numbers[i]) < 0.0) {
				return "Négatif interdits : " + Double.parseDouble(numbers[i]);
			}
			sum += Double.parseDouble(numbers[i]);
		}
		
		String res = String.format(Locale.US, "%.1f", sum);
		res.replace(".", ",");
		
		return res;
	}
	
	private static String customAdditionator(String input, String sep) {
		String[] numbers;
		
		if (sep.equals("|")) {
			numbers = input.split("\\" + sep);
		} else {
			numbers = input.split(sep);
		}
		
		numbers[1] = numbers[1].substring(2);
		
		double sum = 0;
		for (int i = 1; i < numbers.length; i++) {
			if (numbers[i].contains(",")) {
				return separatorException(numbers, i, sep);
			}
			if (numbers[i].equals("")) {
				return numberSeparatorException(numbers, i);
			} else if (Double.parseDouble(numbers[i]) < 0.0) {
				return "Négatif interdits : " + Double.parseDouble(numbers[i]);
			}
			sum += Double.parseDouble(numbers[i]);
		}
		
		return sum + "";
	}
	
	private static String numberSeparatorException(String[] numbers, int i) {
		int pos = i;
		for (int j = 0; j < i; j++) {
			pos += numbers[j].length();
		}
		return "Nombre attendu à la place de \"\\n\" à la position " + pos;
	}
	
	private static String separatorException(String[] numbers, int i, String sep) {
		return "";
	}
	
}
