package tp4;

public class FooBarQix {

	private static StringBuilder builder = new StringBuilder();
	
	public static String fooBarQix(int number) {
		builder.delete(0, builder.capacity());
		
		boolean nothing = true;
		String numberStr = number + "";
		
		nothing = checkDivide(number, nothing);
		nothing = checkContains(numberStr, nothing);
		
		if (nothing) {
			builder.append(number);
		}
		
		return builder.toString();
	}
	
	private static boolean checkDivide(int number, boolean nothing) {
		if (number % 3 == 0) {
			nothing = false;
			builder.append("Foo");
		}
		if (number % 5 == 0) {
			nothing = false;
			builder.append("Bar");
		}
		if (number % 7 == 0) {
			nothing = false;
			builder.append("Qix");
		}
		return nothing;
	}
	
	private static boolean checkContains(String string, boolean nothing) {
		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) == '3') {
				nothing = false;
				builder.append("Foo");
			}
			if (string.charAt(i) == '5') {
				nothing = false;
				builder.append("Bar");
			}
			if (string.charAt(i) == '7') {
				nothing = false;
				builder.append("Qix");
			}
		}
		
		return nothing;
	}
	
}
